/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miraclesoft.utilities;

/**
 *
 * @author ppalaparthi1
 */
import com.miraclesoft.entities.Node;
import com.miraclesoft.entities.NodeList;
import java.util.Properties;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;

public class ConfigUtil {

    private Node nodeConfig;

    private NodeList nodeList;

    public Node getNodeConfig() {
        return nodeConfig;
    }

    public void setNodeConfig(Node nodeConfig) {
        this.nodeConfig = nodeConfig;
    }

    public Node getNodeByIpAddress(String ipAddress) {
        MainClass mc = new MainClass();
        Node resObj = mc.nodeGet("http://172.17.12.137:8731/node/Ip/" + ipAddress);
        System.out.println("resObj--- " + resObj.getIpAddress());
        saveToPropFile("C:/Users/ppalaparthi1/Documents/NetBeansProjects/dmaasMFT/Resources/config.properties", resObj);
        return resObj;
    }

    public Node getNodeByNodeName(String nodeName) {

        MainClass mc = new MainClass();
        Node resObj = mc.nodeGet("http://172.17.12.137:8731/node/Name/" + nodeName);
        System.out.println("resObj--- " + resObj.getPortNumber());
        saveToPropFile("C:/Users/ppalaparthi1/Documents/NetBeansProjects/dmaasMFT/Resources/config.properties", resObj);
        return resObj;

    }

    public void saveToPropFile(String propFileName, Node resObj) {

        Properties props = new Properties();
        props.setProperty("ipAddress", resObj.getIpAddress());
        props.setProperty("portNumber", "" + resObj.getPortNumber());
        props.setProperty("nodeType", resObj.getNodeType());
        props.setProperty("nodeName", resObj.getNodeName());
        OutputStream os = null;
        try {
            os = new FileOutputStream(propFileName);
            props.store(os, null);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void readFromPropFile(String propFileName) {

        Properties props = new Properties();
        FileInputStream fis=null;
        try {
            fis = new FileInputStream(propFileName);
            props.load(fis);
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("ipAddress---"+props.getProperty("ipAddress"));
        System.out.println("portNumber---"+props.getProperty("portNumber"));
        System.out.println("nodeType---"+props.getProperty("nodeType"));
        System.out.println("nodeName---"+props.getProperty("nodeName"));
    }

    public NodeList getAllNodesFromConfigServer() {

        //API call to get configuration of a node and populate the above NodeList field
        MainClass mc = new MainClass();
        mc.nodeGet("http://172.17.12.137:8731/node/");
        return null;

    }

    public void saveAllNodesToPropFile(String propFile) {

        //save the NodeList to Prop File
    }

    public void readAllNodesFromPropFile(String propFile) {

        //read all the Nodes' prop from file and update NodeList object
    }

}
