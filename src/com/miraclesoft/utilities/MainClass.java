/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miraclesoft.utilities;

/**
 * +
 *
 *
 * @author ppalaparthi1
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import com.miraclesoft.entities.Node;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.miraclesoft.entities.NodeList;
import com.miraclesoft.fileAgent.FileListener;

public class MainClass {

    public static void main(String[] args) throws Exception {
        String configFilePath ="C:/Users/ppalaparthi1/Documents/NetBeansProjects/dmaasMFT/Resources/config.properties";
        String pathToBeListened = "C:/Users/ppalaparthi1/Desktop/fileListen";
        ConfigUtil config = new ConfigUtil();
        NodeList nodeList = new NodeList();
        FileListener fileListener = new FileListener();
        fileListener.listen(pathToBeListened, configFilePath);
//       config.getNodeByIpAddress("172.17.12.137");
//       config.getNodeByNodeName("ppalaparthi1");
//       config.getAllNodesFromConfigServer();
//       nodeList.isValid("172.17.12.137");
//       config.readFromPropFile(configFilePath);
    }

    public String checkNode(String restApi) {
         Node nodeObject = null;
         StringBuffer response = new StringBuffer();
        try {
            URL url = new URL(restApi);//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            
            while ((output = br.readLine()) != null) {
                response.append(output);
//                System.out.println(output);
            }

//            System.out.println(response);
            conn.disconnect();

        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
        }
        return response.toString();
    }

    public Node nodeGet(String restApi) {
        Node nodeObject = null;
        try {

            URL url = new URL(restApi);//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            StringBuffer response = new StringBuffer();
            while ((output = br.readLine()) != null) {
                response.append(output);
                System.out.println(output);
            }

            ObjectMapper mapper = new ObjectMapper();
            nodeObject = mapper.readValue(response.toString(), Node.class);
            System.out.println(nodeObject.toString());
            conn.disconnect();

        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
        }
        return nodeObject;
    }
}
