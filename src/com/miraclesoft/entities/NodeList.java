/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miraclesoft.entities;

/**
 *
 * @author ppalaparthi1
 */
import com.miraclesoft.utilities.MainClass;
import java.util.ArrayList;
import java.util.List;

public class NodeList {

    private static NodeList nodeListObject = null;

    private List<Node> listOfNodes;

    public NodeList() {

    }

    public static NodeList getInstance() {

        if (nodeListObject == null) {
            return new NodeList();
        }
        return nodeListObject;
    }

    protected List<Node> getListOfNodes() {
        return this.listOfNodes;
    }

    protected void setListOfNodes(List<Node> listOfNodes) {
        this.listOfNodes = listOfNodes;
    }

    public boolean isValid(String ipAddress) {
        MainClass mc = new MainClass();
        String resObj=mc.checkNode("http://172.17.12.137:8731/node/ipCheck/" + ipAddress);
        if(resObj.equalsIgnoreCase("True"))
            System.out.println("Node exists ... ");
        else
            System.out.println("No such Node exists ... ");
        return false;
    }

    public void addNode(Node node) {

    }

    public void updateNode(Node node) {

    }
}
