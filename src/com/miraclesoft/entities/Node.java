/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miraclesoft.entities;

/**
 *
 * @author ppalaparthi1
 */
public class Node {

    private String ipAddress;

    private int portNumber;

    private String nodeType;

    private String nodeName;

//    public Node(String ipAddress, int portNumber, String nodeType, String nodeName) {
//        this.ipAddress = ipAddress;
//        this.portNumber = portNumber;
//        this.nodeType = nodeType;
//        this.nodeName = nodeName;
//    }

    @Override
    public String toString() {
        return "{ipAddress=" + ipAddress + ", portNumber=" + portNumber + ", nodeType=" + nodeType + ", nodeName=" + nodeName + '}';
    }
    
    
    

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

}
