/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miraclesoft.fileAgent;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Properties;

/**
 *
 * @author ppalaparthi1
 */
public class FileListener {

    public void listen(String pathToBeListened, String configPath) {
        WatchService watchService;
        try {
            System.out.println("111111");
            watchService = FileSystems.getDefault().newWatchService();
//            System.out.println("222222222");
            Path path = Paths.get(pathToBeListened);
//            System.out.println("333333333333");
            path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE,
                    StandardWatchEventKinds.ENTRY_MODIFY);
//            System.out.println("444444444444");
            WatchKey key;
            while ((key = watchService.take()) != null) {
//                System.out.println("555555");
                for (WatchEvent<?> event : key.pollEvents()) {
                    System.out.println("Event kind:" + event.kind() + ". File affected: " + event.context() + ".");
                    // read the config properties and call the Utility accordingly
                    Properties props = new Properties();
                    FileInputStream fis = null;
                    try {
                        fis = new FileInputStream(configPath);
                        props.load(fis);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    System.out.println("ipAddress---" + props.getProperty("ipAddress"));
                    System.out.println("portNumber---" + props.getProperty("portNumber"));
                    System.out.println("nodeType---" + props.getProperty("nodeType"));
                    System.out.println("nodeName---" + props.getProperty("nodeName"));
//                    String ipAddress = props.getProperty("ipAddress");
//                    String portNumber = props.getProperty("portNumber");
//                    String nodeType = props.getProperty("nodeType");
//                    String nodeName = props.getProperty("nodeName");
//                  DesktopFileUtil.uploadFile(ipAddress, portNumber, nodeType, nodeName);
                    break;
                }
                key.reset();
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
